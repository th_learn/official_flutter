int change(int i, int change_f(int i)) {
  return change_f(i);
}

int increment(int i) {
  return i + 1;
}

void main () {
  print(change(1, increment));
}