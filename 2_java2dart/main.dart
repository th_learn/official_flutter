import 'dart:math';

class Rectangle {
  Point origin;
  int width;
  int height;

  Rectangle({this.origin = const Point(0, 0), this.width = 0, this.height = 0});

  @override
  String toString() {
    return 'Origin: (${origin.x}, ${origin.y}),  width: $width, height: $height';
  }


}

class Bicycle {
  int cadence;
  int _speed;
  int gear;

  Bicycle(this.cadence, this.gear);

  int get speed => _speed;

  void applyBrake(int decrements) => _speed -= decrements;

  void speedUp(int decrements) => _speed += decrements;

  @override
  String toString() => 'Bicycle{speed: $speed} mph';
}

void main() {
  final bike = Bicycle(2, 1);
  print(bike);

  print(Rectangle(origin: const Point(10, 20), width: 100, height: 200));
  print(Rectangle(origin: const Point(10, 10)));
  print(Rectangle(width: 200));
  print(Rectangle());
}